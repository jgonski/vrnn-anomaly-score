# VRNN Anomaly Score

Currently needs to be run on katya01 to access data

pytorch and scikit-hep must be installed

This package includes:

  -data/BoostLoop.py - Jet pre-processing script

  -training/train.py - Training script that trains and evaluates the model with a contaminated training set (JZ5W + TTBar), uncontaminated validation set (JZ5W), and anomaly set (TTBar)

  -training/model.py - The VRNN Model

  -training.saves.vrnn_state_dict_pretrained.pth - Pre-trained model weights


Usage:


cd data/

python BoostLoop.py


cd training/

python train.py
