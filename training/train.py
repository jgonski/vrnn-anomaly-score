import torch.nn as nn
import torch.utils
import torch.utils.data
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt 
from model import VRNN
import numpy as np
import pickle
import h5py

import sys, os
from sklearn.metrics import roc_curve, auc

import time


#Pre-processed jet hdf5 directory
#dataDir = '/data/users/akahn/VRNN_Data/HDF5/'
dataDir = sys.path[0]+'/../data/'

maxconsts = 10
rocname = "test_"+str(maxconsts)

if not os.path.exists(sys.path[0]+"/plots/"+rocname):
  try:
    os.makedirs(sys.path[0]+"/plots/"+rocname)
    os.makedirs(sys.path[0]+"/plots/"+rocname+"/roc")
    os.makedirs(sys.path[0]+"/plots/"+rocname+"/scores")
  except OSError as exc: # Guard against race condition
    if exc.errno != errno.EEXIST:
      raise

def get_data(infile = None, anomfile = None, n_const = None, contamination=0.05):
    #Retrieves the dictionaries of constituent and HLV information from the training set
    #In the anomaly-detection setup, returns a contaminated dataset of the normal jets with the anomalous, i.e. signal, jets
    #Also computs the means and standard deviations of each high-level variable
    #Each of the high-level variables in any input will subtract the means and divide by the standard deviations found from the training set

    if(infile is None): infile = h5py.File(dataDir+"JZ5W_Training_NewHLV_"+str(maxconsts)+"C_Norm_VRNN.hdf5", "r+")
    if(anomfile is None): anomfile = h5py.File(dataDir+"TTBar_Anomaly_NewHLV_"+str(maxconsts)+"C_Norm_VRNN.hdf5", "r+")
    consts_final = dict()
    hlvs_final = dict()
    labels_final = dict()
    all_hlvs = []
    for n_c in infile.keys():
      if(n_c == "0"): continue
      print(n_c)
      n_normal = len(infile[n_c+"/hlvs"][()])
      n_contam = int(contamination*n_normal/(1-contamination))
      if(n_normal > 0):
        for i in range(n_normal):
          all_hlvs.append(infile[n_c+"/hlvs"][(i)])
      if(len(anomfile[n_c+"/hlvs"][()]) > n_contam):
        for i in range(n_contam):
          all_hlvs.append(anomfile[n_c+"/hlvs"][(i)])
      else:
        for i in range(len(anomfile[n_c+"/hlvs"][()])):
          all_hlvs.append(anomfile[n_c+"/hlvs"][(i)])
        print("Not enough anomaly jets", len(anomfile[n_c+"/hlvs"][()]), n_normal, n_contam, "True Contamination: ", len(anomfile[n_c+"/hlvs"][()])/(n_normal + len(anomfile[n_c+"/hlvs"][()])))
    all_hlvs = np.array(all_hlvs)

    hlv_means = np.mean(all_hlvs, axis=0)
    hlv_stds = np.std(all_hlvs, axis=0)
    for n_c in infile.keys():
      if(n_c == "0"): continue
      print(n_c)
      tmp_consts = []
      tmp_hlvs = []
      tmp_labels = []
      n_normal = len(infile[n_c+"/constituents"][()])
      n_contam = int(contamination*n_normal/(1-contamination))
      if(n_normal > 0):
        tmp_consts = infile[n_c+"/constituents"][()]
        tmp_hlvs = infile[n_c+"/hlvs"][()]
        tmp_labels = np.zeros(n_normal)
      tmp_anom_consts = []
      tmp_anom_hlvs = []
      tmp_anom_labels = []
      if(len(anomfile[n_c+"/constituents"][()]) > n_contam):
        tmp_anom_labels = np.ones(n_contam)
        for i in range(n_contam):
          tmp_anom_consts.append(anomfile[n_c+"/constituents"][(i)])
          tmp_anom_hlvs.append(anomfile[n_c+"/hlvs"][(i)])
      else:
        tmp_anom_labels = np.ones(len(anomfile[n_c+"/constituents"][()]))
        for i in range(len(anomfile[n_c+"/constituents"][()])):
          tmp_anom_consts.append(anomfile[n_c+"/constituents"][(i)])
          tmp_anom_hlvs.append(anomfile[n_c+"/hlvs"][(i)])
        print("Not enough anomaly jets", len(anomfile[n_c+"/hlvs"][()]), n_normal, n_contam, "True Contamination: ", len(anomfile[n_c+"/hlvs"][()])/(n_normal + len(anomfile[n_c+"/hlvs"][()])))
      print("Consts:", np.shape(tmp_consts), np.shape(tmp_anom_consts))
      print("HLVs:", np.shape(tmp_hlvs), np.shape(tmp_anom_hlvs))
      print("Labels:", np.shape(tmp_labels), np.shape(tmp_anom_labels))
      consts = np.concatenate((tmp_consts, tmp_anom_consts), axis=0)
      hlvs = np.concatenate((tmp_hlvs, tmp_anom_hlvs), axis=0)
      labels = np.concatenate((tmp_labels, tmp_anom_labels), axis=0)
      print("Consts Final:", np.shape(consts))
      print("HLVs Final:", np.shape(hlvs))
      print("Labels Final:", np.shape(labels))
        
      hlvs = (hlvs - hlv_means)/hlv_stds

      n_jets = len(consts)
      rand_indeces = np.random.permutation(n_jets)
      consts = consts[rand_indeces]
      hlvs = hlvs[rand_indeces]
      labels = labels[rand_indeces]


      consts_final.update({n_c:consts})
      hlvs_final.update({n_c:hlvs})
      labels_final.update({n_c:labels})
   


    return consts_final, hlvs_final, hlv_means, hlv_stds, labels_final









def get_val_data(hlv_means, hlv_stds, infile = None, n_const = None):
    #Retrieves the dictionaries of constituent and HLV information from the validation set

    if(infile is None): infile = h5py.File(dataDir+"JZ5W_Validation_NewHLV_"+str(maxconsts)+"C_Norm_VRNN.hdf5", "r+")
    data = dict()
    hlvs = dict()
    for n_c in infile.keys():
      if(len(infile[n_c+"/constituents"][()]) > 0):
        tmp_consts = infile[n_c+"/constituents"][()]
        #data.update({n_c: infile[n_c+"/constituents"]})
        data.update({n_c: tmp_consts})
      if(len(infile[n_c+"/hlvs"][()]) > 0):
        hlvs.update({n_c: (infile[n_c+"/hlvs"][()] - hlv_means)/hlv_stds})
      else:
        hlvs.update({n_c: infile[n_c+"/hlvs"]})
    infile.close()
    return data, hlvs




def get_anom_data(hlv_means, hlv_stds, infile = None, n_const = None):
    #Retrieves the dictionaries of constituent and HLV information from the anomaly set

    if(infile is None): infile = h5py.File(dataDir+"TTBar_Anomaly_NewHLV_"+str(maxconsts)+"C_Norm_VRNN.hdf5", "r+")
    data = dict()
    hlvs = dict()
    for n_c in infile.keys():
      if(len(infile[n_c+"/constituents"][()]) > 0):
        tmp_consts = infile[n_c+"/constituents"][()]
        #data.update({n_c: infile[n_c+"/constituents"]})
        data.update({n_c: tmp_consts})
      if(len(infile[n_c+"/hlvs"][()]) > 0):
        hlvs.update({n_c: (infile[n_c+"/hlvs"][()] - hlv_means)/hlv_stds})
      else:
        hlvs.update({n_c: infile[n_c+"/hlvs"]})
    infile.close()
    return data, hlvs


def ktd(c1, c2):
  #Returns the kt distance between two constituents
  power=1
  return np.minimum(np.power(c1[0], 2*power), np.power(c2[0], 2*power))*(np.square(c1[1] - c2[1]) + np.square(c1[2] - c2[2]))

def min_kt(jet1, jet2):
  #Computes, to first order, the minimum kt distance between two jets
  minkt = 0
  ktds = np.zeros(shape=[len(jet1), len(jet2)])
  for i in range(len(jet1)):
    for j in range(len(jet2)):
      ktds[i][j] = ktd(jet1[i], jet2[j])
  for i in range(len(jet1)):
    idxs = np.unravel_index(np.argmin(ktds, axis=None), ktds.shape)
    minkt += ktds[idxs]
    ktds = np.delete(np.delete(ktds, idxs[0], 0), idxs[1], 1)
  return minkt

def get_batches(data, batch_size):
    n_batches = int(len(data)/batch_size)
    #n_batches = 1
    batched_data = np.empty(shape=[n_batches, batch_size, np.shape(data)[1], np.shape(data)[2]])
    for batch in range(n_batches):
      batched_data[batch] = data[batch*batch_size:(batch+1)*batch_size]
    #print(np.shape(batched_data))
    return batched_data

def get_hlv_batches(data, batch_size):
    n_batches = int(len(data)/batch_size)
    #n_batches = 1
    batched_data = np.empty(shape=[n_batches, batch_size, np.shape(data)[1]])
    for batch in range(n_batches):
      batched_data[batch] = data[batch*batch_size:(batch+1)*batch_size]
    #print(np.shape(batched_data))
    return batched_data


def train(epoch):
  #Trains the model

  train_loss = 0
  total_batches = 0
  for n_c in batched_data_train.keys():
    if(int(n_c) in const_list):
      start = time.time()
      batched_data = torch.tensor(batched_data_train[n_c]).float().cuda()
      batched_hlvs = torch.tensor(batched_hlvs_train[n_c]).float().cuda()
      total_batches += len(batched_data)
      for batch_idx in range(len(batched_data)):
        batch_start = time.time()
        data = batched_data[batch_idx] 
        hlvs = batched_hlvs[batch_idx]
        
        data = Variable(data.transpose(0, 1))
        optimizer.zero_grad()
        kld_loss, nll_loss, loss, y_mean = model(data, hlvs)
        loss.backward()
        optimizer.step()

        #grad norm clipping, only in pytorch version >= 1.10
        nn.utils.clip_grad_norm(model.parameters(), clip)

        train_loss += loss.data

      
  print('====> Epoch: {} Average loss: {:.4f}'.format(
    epoch, train_loss / (total_batches)))
  train_loss /= total_batches
  return train_loss.cpu().numpy()

def test(epoch):
  #Tests the model using batched validation and anomaly data 
  
  v_loss = 0
  total_batches = 0
  for n_c in batched_data_val.keys():
    if(int(n_c) in const_list):
      batched_val = torch.tensor(batched_data_val[n_c]).float().cuda()
      batched_hlv_val = torch.tensor(batched_hlvs_val[n_c]).float().cuda()
      total_batches += len(batched_val)
      for i in range(len(batched_val)):
        data = batched_val[i]
        hlvs = batched_hlv_val[i]
        
        data = Variable(data.transpose(0, 1))
        kld_loss, nll_loss, loss, y_mean = model(data, hlvs)
        v_loss += loss.data

  v_loss /= total_batches 

  a_loss = 0
  total_batches = 0
  for n_c in batched_data_anom.keys():
    if(int(n_c) in const_list):
      batched_anom = torch.tensor(batched_data_anom[n_c]).float().cuda()
      batched_hlv_anom = torch.tensor(batched_hlvs_anom[n_c]).float().cuda()
      total_batches += len(batched_anom)
      for i in range(len(batched_anom)):
        data = batched_anom[i]
        hlvs = batched_hlv_anom[i]
        
        data = Variable(data.transpose(0, 1))
        kld_loss, nll_loss, loss, y_mean = model(data, hlvs)
        a_loss += loss.data 

  a_loss /= total_batches
  return (v_loss, a_loss)

def evaluate(eval_data, eval_hlvs, epoch="", sample="", labels=None):
  #Evaluates the model jet-by-jet


  eval_const_list = const_list
  mean_kld_loss, mean_nll_loss = 0, 0
  losses = dict()
  all_losses = []
  losses_minkt = dict()
  all_losses_minkt = []
  losses_kldmkt = dict()
  all_losses_kldmkt = []
  output_labels = []
  for n_c in eval_data.keys():
    if not int(n_c) in eval_const_list: continue
    tmp_losses = []
    tmp_losses_minkt = []
    tmp_losses_kldmkt = []
    print(epoch+" "+sample+", "+n_c + " Constituents")
    jets = torch.tensor(eval_data[n_c]).float().cuda()
    hlv = torch.tensor(eval_hlvs[n_c]).float().cuda()
    for i in range(minjets[n_c]):
      if not labels == None:
        output_labels.append(labels[n_c][i])
      data = jets[i].unsqueeze(0)
      hlvs = hlv[i].unsqueeze(0)
      
      data = Variable(data.transpose(0, 1))
      kld_loss, nll_loss, _, y_mean = model(data, hlvs)
      mean_kld_loss += kld_loss.data
      mean_nll_loss += nll_loss.data
      minkt = min_kt(data.cpu().transpose(0, 1)[0].numpy(), y_mean.cpu().transpose(0, 1)[0].numpy())
      loss = 1000*kld_loss
      loss_minkt = minkt
      loss_kldmkt = 1000*kld_loss.data.cpu().numpy() + minkt
      if(i % 100 == 0): print(epoch+" "+sample+", "+n_c+":",i, loss.data.cpu().numpy())
      tmp_losses.append(loss.data.cpu().numpy())
      all_losses.append(loss.data.cpu().numpy())
      tmp_losses_minkt.append(loss_minkt)
      all_losses_minkt.append(loss_minkt)
      tmp_losses_kldmkt.append(loss_kldmkt)
      all_losses_kldmkt.append(loss_kldmkt)
    losses.update({n_c: tmp_losses})
    losses_minkt.update({n_c: tmp_losses_minkt})
    losses_kldmkt.update({n_c: tmp_losses_kldmkt})
  print("Mean Evaluation Score: ", np.mean(all_losses))
  if not labels == None: return all_losses, losses, all_losses_minkt, losses_minkt, all_losses_kldmkt, losses_kldmkt, output_labels
  else: return all_losses, losses, all_losses_minkt, losses_minkt, all_losses_kldmkt, losses_kldmkt



#hyperparameters
x_dim = 3 #3 features per constituent [pt, eta, phi]
hlv_dim = 16 #Number of high-level variables
h_dim = 100 #Hidden layer dimensionality
z_dim = 16 #Latent space dimensionality
n_layers =  1 #Number of layers in the recurrence relation that updates the hidden state
n_epochs  = 1 #Number of training epochs
clip = 10 #Maximum allowed value of the loss gradient. Gradients higher than this value will be clipped
learning_rate = 1e-6 
l2_norm = 0 #Weight decay parameter
batch_size = 100 
seed = 128
print_every = 100
save_every = 1

save_images = False


print("CUDA Available:", torch.cuda.is_available())
print("CUDA Device:", torch.cuda.get_device_name())


#manual seed
torch.manual_seed(seed)

data_train, hlvs_train, hlv_means, hlv_stds, training_labels = get_data()
data_val, hlvs_val = get_val_data(hlv_means, hlv_stds)
data_anom, hlvs_anom = get_anom_data(hlv_means, hlv_stds)

batched_data_train = dict()
batched_data_val = dict()
batched_data_anom = dict()
batched_hlvs_train = dict()
batched_hlvs_val = dict()
batched_hlvs_anom = dict()

for i in range(5, 80):
  if str(i) in data_train.keys() and len(data_train[str(i)]) > 0: batched_data_train.update({str(i): get_batches(data_train[str(i)], batch_size)})
  if str(i) in data_val.keys() and len(data_val[str(i)]) > 0: batched_data_val.update({str(i): get_batches(data_val[str(i)], batch_size)})
  if str(i) in data_anom.keys() and len(data_anom[str(i)]) > 0: batched_data_anom.update({str(i): get_batches(data_anom[str(i)], batch_size)})
  if str(i) in hlvs_train.keys() and len(hlvs_train[str(i)]) > 0: batched_hlvs_train.update({str(i): get_hlv_batches(hlvs_train[str(i)], batch_size)})
  if str(i) in hlvs_val.keys() and len(hlvs_val[str(i)]) > 0: batched_hlvs_val.update({str(i): get_hlv_batches(hlvs_val[str(i)], batch_size)})
  if str(i) in hlvs_anom.keys() and len(hlvs_anom[str(i)]) > 0: batched_hlvs_anom.update({str(i): get_hlv_batches(hlvs_anom[str(i)], batch_size)})


#batched_val = get_batches(data_val_5, batch_size)
#batched_anom = get_batches(data_anom_5, batch_size)


#List of numbers-of-constituents to be tested/evaluated on
const_list = range(5, maxconsts+1)


minjets = dict()
for i in const_list:
  minjets.update({str(i): int(min(len(data_train[str(i)]), len(data_val[str(i)]), len(data_anom[str(i)]))/10)})

losses_train = []
losses_val = []
losses_anom = []

#Model initialization
model = VRNN(x_dim, hlv_dim, h_dim, z_dim, n_layers)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=l2_norm)
do_train=True
#do_train=False

#Loading weights from pre-training
state_dict = torch.load('saves/vrnn_state_dict_pretrained.pth')
model.load_state_dict(state_dict)

if(do_train):
  for epoch in range(1, n_epochs + 1):
    l_train = 0
    l_val = 0
    l_anom = 0 
    #training + testing
    start_time = time.time()
    l_train = train(epoch)
    l_val, l_anom = test(epoch)
    print("Time: ", time.time() - start_time)
    print("Memory Usage: ", torch.cuda.memory_allocated('cuda:0')/1e9)
    losses_train.append(l_train)
    losses_val.append(l_val)
    losses_anom.append(l_anom)
    
    scores_normal, scores_normal_dict,     scores_minkt_normal, scores_minkt_normal_dict,   scores_kldmkt_normal, scores_kldmkt_normal_dict, normal_labels    = evaluate(data_train, hlvs_train, epoch=str(epoch), sample="Training", labels=training_labels)
    scores_val, scores_val_dict,           scores_minkt_val, scores_minkt_val_dict,         scores_kldmkt_val, scores_kldmkt_val_dict          = evaluate(data_val, hlvs_val, epoch=str(epoch), sample="Validation")
    scores_anom, scores_anom_dict,         scores_minkt_anom, scores_minkt_anom_dict,       scores_kldmkt_anom, scores_kldmkt_anom_dict        = evaluate(data_anom, hlvs_anom, epoch=str(epoch), sample="Anomaly")

    scores_normal = 1-np.exp(np.multiply(scores_normal, -1))
    scores_val = 1-np.exp(np.multiply(scores_val, -1))
    scores_anom = 1-np.exp(np.multiply(scores_anom, -1))


    scores_minkt_normal = 1-np.exp(np.multiply(scores_minkt_normal, -1))
    scores_minkt_val = 1-np.exp(np.multiply(scores_minkt_val, -1))
    scores_minkt_anom = 1-np.exp(np.multiply(scores_minkt_anom, -1))

    scores_kldmkt_normal = 1-np.exp(np.multiply(scores_kldmkt_normal, -1))
    scores_kldmkt_val = 1-np.exp(np.multiply(scores_kldmkt_val, -1))
    scores_kldmkt_anom = 1-np.exp(np.multiply(scores_kldmkt_anom, -1))


    scores_all = scores_normal
    #scores_all = np.append(scores_all, scores_anom)
    scores_all_val = scores_val
    scores_all_val = np.append(scores_all_val, scores_anom)

    scores_minkt_all = scores_minkt_normal
    #scores_minkt_all = np.append(scores_minkt_all, scores_minkt_anom)
    scores_minkt_all_val = scores_minkt_val
    scores_minkt_all_val = np.append(scores_minkt_all_val, scores_minkt_anom)

    scores_kldmkt_all = scores_kldmkt_normal
    #scores_kldmkt_all = np.append(scores_kldmkt_all, scores_kldmkt_anom)
    scores_kldmkt_all_val = scores_kldmkt_val
    scores_kldmkt_all_val = np.append(scores_kldmkt_all_val, scores_kldmkt_anom)


    #labels = np.append(np.zeros(len(scores_normal)), np.ones(len(scores_anom)))
    labels = normal_labels
    labels_val = np.append(np.zeros(len(scores_val)), np.ones(len(scores_anom)))

    fpr, tpr, _ = roc_curve(labels, scores_all)
    fpr_v, tpr_v, _ = roc_curve(labels_val, scores_all_val)

    fpr_minkt, tpr_minkt, _ = roc_curve(labels, scores_minkt_all)
    fpr_minkt_v, tpr_minkt_v, _ = roc_curve(labels_val, scores_minkt_all_val)

    fpr_kldmkt, tpr_kldmkt, _ = roc_curve(labels, scores_kldmkt_all)
    fpr_kldmkt_v, tpr_kldmkt_v, _ = roc_curve(labels_val, scores_kldmkt_all_val)


    roc_auc = auc(fpr, tpr) # compute area under the curve
    roc_auc_v = auc(fpr_v, tpr_v) # compute area under the curve

    roc_auc_minkt = auc(fpr_minkt, tpr_minkt) # compute area under the curve
    roc_auc_minkt_v = auc(fpr_minkt_v, tpr_minkt_v) # compute area under the curve

    roc_auc_kldmkt = auc(fpr_kldmkt, tpr_kldmkt) # compute area under the curve
    roc_auc_kldmkt_v = auc(fpr_kldmkt_v, tpr_kldmkt_v) # compute area under the curve


    scores_training_normal = []
    scores_training_anom = []
    scores_minkt_training_normal = []
    scores_minkt_training_anom = []
    scores_kldmkt_training_normal = []
    scores_kldmkt_training_anom = []
    for i in range(len(scores_all)):
      if(int(labels[i]) == int(0)): 
        #print("Label = 0")
        scores_training_normal.append(scores_all[i])
        scores_minkt_training_normal.append(scores_minkt_all[i])
        scores_kldmkt_training_normal.append(scores_kldmkt_all[i])
      elif(int(labels[i]) == int(1)): 
        #print("Label = 1")
        scores_training_anom.append(scores_all[i])
        scores_minkt_training_anom.append(scores_minkt_all[i])
        scores_kldmkt_training_anom.append(scores_kldmkt_all[i])
      else: print("label neither 0 or 1", int(labels[i]))



    np.save("plots/"+rocname+"/roc/fpr_train_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr)
    np.save("plots/"+rocname+"/roc/tpr_train_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr)
    np.save("plots/"+rocname+"/roc/fpr_v_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_v)
    np.save("plots/"+rocname+"/roc/tpr_v_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_v)

    np.save("plots/"+rocname+"/roc/fpr_minkt_train_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_minkt)
    np.save("plots/"+rocname+"/roc/tpr_minkt_train_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_minkt)
    np.save("plots/"+rocname+"/roc/fpr_minkt_v_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_minkt_v)
    np.save("plots/"+rocname+"/roc/tpr_minkt_v_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_minkt_v)

    np.save("plots/"+rocname+"/roc/fpr_kldmkt_train_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_kldmkt)
    np.save("plots/"+rocname+"/roc/tpr_kldmkt_train_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_kldmkt)
    np.save("plots/"+rocname+"/roc/fpr_kldmkt_v_all_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_kldmkt_v)
    np.save("plots/"+rocname+"/roc/tpr_kldmkt_v_all_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_kldmkt_v)

    np.save("plots/"+rocname+"/scores/scores_normal_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_normal)
    np.save("plots/"+rocname+"/scores/scores_training_normal_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_training_normal)
    np.save("plots/"+rocname+"/scores/scores_training_anom_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_training_anom)
    np.save("plots/"+rocname+"/scores/scores_val_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_val)
    np.save("plots/"+rocname+"/scores/scores_anom_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_anom)

    np.save("plots/"+rocname+"/scores/scores_minkt_normal_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_normal)
    np.save("plots/"+rocname+"/scores/scores_minkt_training_normal_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_training_normal)
    np.save("plots/"+rocname+"/scores/scores_minkt_training_anom_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_training_anom)
    np.save("plots/"+rocname+"/scores/scores_minkt_val_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_val)
    np.save("plots/"+rocname+"/scores/scores_minkt_anom_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_minkt_anom)

    np.save("plots/"+rocname+"/scores/scores_kldmkt_normal_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_normal)
    np.save("plots/"+rocname+"/scores/scores_kldmkt_training_normal_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_training_normal)
    np.save("plots/"+rocname+"/scores/scores_kldmkt_training_anom_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_training_anom)
    np.save("plots/"+rocname+"/scores/scores_kldmkt_val_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_val)
    np.save("plots/"+rocname+"/scores/scores_kldmkt_anom_all_"+rocname+"_epoch_"+str(epoch)+".npy", scores_kldmkt_anom)





    print("Epoch: "+str(epoch)+": Max "+str(maxconsts)+" Constituents Evaluation:")
    print("Training AUC:", roc_auc)
    print("Validation AUC:", roc_auc_v)

    for n_c in scores_normal_dict.keys():

      scores_normal = 1-np.exp(np.multiply(scores_normal_dict[n_c], -1))
      scores_val = 1-np.exp(np.multiply(scores_val_dict[n_c], -1))
      scores_anom = 1-np.exp(np.multiply(scores_anom_dict[n_c], -1))


      scores_all = scores_normal
      scores_all = np.append(scores_all, scores_anom)

      scores_all_val = scores_val
      scores_all_val = np.append(scores_all_val, scores_anom)

      labels = np.append(np.zeros(len(scores_normal)), np.ones(len(scores_anom)))
      labels_val = np.append(np.zeros(len(scores_val)), np.ones(len(scores_anom)))

      fpr, tpr, _ = roc_curve(labels, scores_all)
      fpr_v, tpr_v, _ = roc_curve(labels_val, scores_all_val)

      roc_auc = auc(fpr, tpr) # compute area under the curve
      roc_auc_v = auc(fpr_v, tpr_v) # compute area under the curve



      np.save("plots/"+rocname+"/roc/fpr_train_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", fpr)
      np.save("plots/"+rocname+"/roc/tpr_train_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", tpr)
      np.save("plots/"+rocname+"/roc/fpr_v_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", fpr_v)
      np.save("plots/"+rocname+"/roc/tpr_v_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", tpr_v)

      np.save("plots/"+rocname+"/scores/scores_normal_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", scores_normal)
      np.save("plots/"+rocname+"/scores/scores_val_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", scores_val)
      np.save("plots/"+rocname+"/scores/scores_anom_"+str(n_c)+"_"+rocname+"_epoch_"+str(epoch)+".npy", scores_anom)


    #saving model
    if epoch % save_every == 0:
      fn = 'saves/vrnn_state_dict_'+rocname+'_epoch_'+str(epoch)+'.pth'
      torch.save(model.state_dict(), fn)
      print('Saved model to '+fn)


  #Save training losses
  np.save("plots/"+rocname+"/losses_train_"+rocname+".png", losses_train)
  np.save("plots/"+rocname+"/losses_val_"+rocname+".png", losses_val)
  np.save("plots/"+rocname+"/losses_anom_"+rocname+".png", losses_anom)

