import numpy as np
from skhep.math.vectors import *
from scipy.special import softmax
import matplotlib.pyplot as plt
import pickle
import glob
import h5py

def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    return np.exp(x) / np.sum(np.exp(x), axis=0)

samples_dict = {
                "JZ5W_Training_NewHLV": ["JZ5W_NewHLV_50000.hdf5", "JZ5W_NewHLV_100000.hdf5"],
                "JZ5W_Validation_NewHLV": ["JZ5W_NewHLV_200000.hdf5"],
                "TTBar_Anomaly_NewHLV": ["TTBar_NewHLV_44300.hdf5"]
               }
       


dataDir = "/data/users/akahn/VRNN_Data/HDF5/" 



for n_consts in [10]:
  for sample_name in samples_dict.keys():
  
    processed = []
    processed_flat = []
    
    n_c_hist = []
    n_c_nocut_hist = []
    
    hlvs_total = []
    
    #sorted_constituents = np.zeros(n_consts+1)
    #sorted_constituents = [[]]*n_consts+1
    #print(np.shape(sorted_constituents))
    sorted_constituents={}
    for i in range(n_consts+1):
      sorted_constituents.update({str(i):[]})
    sorted_hlvs={}
    for i in range(n_consts+1):
      sorted_hlvs.update({str(i):[]})
    
    njet = 0
    for sample in samples_dict[sample_name]:
      #data = np.load(sample+".npy", allow_pickle=True)
      #data = np.load(sample, allow_pickle=True)
      infile = h5py.File(dataDir+sample, "r")
      data = infile["constituents"]
      hlvs = infile["hlvs"]
      print(np.shape(data))
      print(np.shape(hlvs))
    
      if(len(hlvs_total) == 0): hlvs_total = hlvs
      else: hlvs_total = np.append(hlvs_total, hlvs, axis=0)
    
      n_jets = len(data)
      #n_jets = 50000
      #for jet_num in range(0, len(data)):
      for jet_num in range(0, n_jets):
      #for jet_num in range(0, 100):
        njet += 1
      
      
        if njet%100 == 0 and njet > 0:
          print(njet)
      
        d = data[jet_num][0]
        hlv_full = hlvs[jet_num]
        #hlv = get_new_hlvs(hlv_full)
        hlv = hlv_full
        
        jet = LorentzVector()
        jet.setptetaphim(d[0], d[2], d[3], d[1])
      
      
        if jet.pt < 150000: 
          print("LowPt jet")
          continue
        
        
        cst_1 = LorentzVector()
        cst_1.setptetaphim(data[jet_num][1][0], data[jet_num][1][2], data[jet_num][1][3], data[jet_num][1][1])
        cst_2 = LorentzVector()
        cst_2.setptetaphim(data[jet_num][2][0], data[jet_num][2][2], data[jet_num][2][3], data[jet_num][2][1])
        
        m_zero = 50000.0 #50GeV Mass
        e_zero = 100000.0 #100GeV Energy
      
        if jet.m <= 50: continue  
        m_rescale = m_zero/jet.m
        
        #Rescale the jet
        jet_rescaled = m_rescale*jet
        #jet_rescaled = LorentzVector()
        #jet_rescaled.setpxpypze(jet.px*m_rescale, jet.py*m_rescale, jet.pz*m_rescale, jet.e*m_rescale)
    
      
        #testvec = jet_rescaled.vector
        
        #print("Mass Rescale")
        #print ([jet_rescaled.pt, jet_rescaled.eta, jet_rescaled.phi(), jet_rescaled.m])
        
        e1 = jet_rescaled.vector.unit()
        x_hat = Vector3D(1, 0, 0)
        e2 = x_hat - (x_hat.dot(e1))*e1
        e3 = e1.cross(e2)
    
        e1 = Vector3D(x=e1[0], y=e1[1], z=e1[2])
        e2 = Vector3D(x=e2[0], y=e2[1], z=e2[2])
        e3 = Vector3D(x=e3[0], y=e3[1], z=e3[2])
        
        #jet_1d = LorentzVector()
        jet_1d = LorentzVector(jet_rescaled.vector.dot(e1), jet_rescaled.vector.dot(e2), jet_rescaled.vector.dot(e3), jet_rescaled.e)
        #jet_1d.setpxpypze(testvec.dot(e1), testvec.dot(e2), testvec.dot(e3), jet_rescaled.e)
        #print("1-D Transformation")
        #print ([jet_1d.pt, jet_1d.eta, jet_1d.phi(), jet_1d.m])
        
        tvec_ref = LorentzVector()
        p_ref = jet_1d.vector
        p_ref = (np.sqrt(  (np.abs(np.square(e_zero) - np.square(jet_1d.m)))/(np.abs(np.square(jet_1d.e) - np.square(jet_1d.m))) ) )*p_ref
        p_ref = Vector3D(p_ref[0], p_ref[1], p_ref[2])
        pt_ref = np.sqrt( np.square(p_ref.x) + np.square(p_ref.y) )
      #  print([pt_ref, jet_1d.eta, jet_1d.phi(), jet_1d.m])
        tvec_ref.setptetaphim(pt_ref, jet_1d.eta, jet_1d.phi(), jet_1d.m)
        
        bp_x = tvec_ref.boostvector.x
        
        beta = (jet_1d.e - (jet_1d.px/bp_x))/(jet_1d.p - (jet_1d.e/bp_x))
        
        jet_boosted=jet_1d.boost(beta, 0, 0)
        
        
        cst_1_rescaled = m_rescale*cst_1
        cst_2_rescaled = m_rescale*cst_2
        
        cst_1_1d = LorentzVector(cst_1_rescaled.vector.dot(e1), cst_1_rescaled.vector.dot(e2), cst_1_rescaled.vector.dot(e3), cst_1_rescaled.e)
        cst_2_1d = LorentzVector(cst_2_rescaled.vector.dot(e1), cst_2_rescaled.vector.dot(e2), cst_2_rescaled.vector.dot(e3), cst_2_rescaled.e)
        
        cst_1_boosted=cst_1_1d.boost(beta, 0, 0)
        cst_2_boosted=cst_2_1d.boost(beta, 0, 0)
        
        
        #Update basis vectors
        
        
        ef_1 = jet_boosted.vector.unit()
        
        ef_2 = cst_1_boosted.vector - ef_1*(cst_1_boosted.vector.dot(ef_1))
        ef_2 = ef_2.unit()
        
        #e3 = e1.cross(ef_2)
        ef_3 = cst_2_boosted.vector - ef_1*(cst_2_boosted.vector.dot(ef_1)) - ef_2*(cst_2_boosted.vector.dot(ef_2))
        ef_3 = ef_3.unit()
        
        
        jet_final = LorentzVector(jet_boosted.vector.dot(ef_1), jet_boosted.vector.dot(ef_2), jet_boosted.vector.dot(ef_3), jet_boosted.e)
        cst_1_final = LorentzVector(cst_1_boosted.vector.dot(ef_1), cst_1_boosted.vector.dot(ef_2), cst_1_boosted.vector.dot(ef_3), cst_1_boosted.e)
        cst_2_final = LorentzVector(cst_2_boosted.vector.dot(ef_1), cst_2_boosted.vector.dot(ef_2), cst_2_boosted.vector.dot(ef_3), cst_2_boosted.e)
      
      
        #constituents = np.zeros(shape=[n_consts, 3])
        constituents = []
        n_c = 0  
        n_c_nocut = 0  
        #Loop through jet constituents
        for j in range(1, n_consts+1):
          row = []
          if(data[jet_num][j][0] < 0.0001):
            #row = [0.0, 0.0, 0.0]
            continue
          else:
            n_c_nocut += 1
            cst = LorentzVector()
            #cst.setptetaphim(data[jet_num][j][0], data[jet_num][j][2], data[jet_num][j][3], data[jet_num][j][1])
            cst.setptetaphim(data[jet_num][j][0], data[jet_num][j][2], data[jet_num][j][3], 0.)
            cst = m_rescale*cst
            cst = LorentzVector(cst.vector.dot(e1), cst.vector.dot(e2), cst.vector.dot(e3), cst.e)
            cst = cst.boost(beta, 0, 0)
            cst = LorentzVector(cst.vector.dot(ef_1), cst.vector.dot(ef_2), cst.vector.dot(ef_3), cst.e)
            #row = [cst.pt, cst.eta, cst.phi()]
            #row = [cst.pt, cst.deltaeta(jet_final), cst.deltaphi(jet_final)]
            #pt = np.sqrt(cst.py**2 + cst.pz**2)
            
            #row = [cst.pt, cst.boostvector.x, cst.boostvector.y]
            dr = np.sqrt(np.square(cst.deltaeta(jet_final)) + np.square(cst.deltaphi(jet_final)))
            if(dr < 1.): 
              #row = [cst.px, (cst.deltaeta(jet_final)+1)/2, (cst.deltaphi(jet_final)+1)/2]
              row = [cst.px/jet_final.px, cst.deltaeta(jet_final), cst.deltaphi(jet_final)]
              if(row[0] > 0.01): #Exclude constituents with less than 1% pt fraction
                n_c += 1
                constituents.append(row)
              #n_c += 1
              #constituents.append(row)
        n_c_hist.append(n_c)
        n_c_nocut_hist.append(n_c_nocut)
            
      
      
      
      
        #Softmax PTs
        pts = np.asarray(constituents).transpose()[0]
        try:
            #soft_pts = np.divide(pts, jet_final.px)
            soft_pts = np.divide(pts, np.sum(pts))
        except ValueError:  
            continue
        for i in range(n_c):
          constituents[i][0] = soft_pts[i]


        #Add to sorted array
        sorted_constituents[str(n_c)].append(constituents)
        sorted_hlvs[str(n_c)].append(hlv)
        #print(constituents)
        #print(np.sum(np.asarray(constituents).transpose()[0]))
    
    outfile = h5py.File("/data/users/jgonski/yXH/boostedJets/"+sample_name+"_"+str(n_consts)+"C_Norm_VRNN.hdf5")
    for i in range(n_consts+1):
      outfile.create_dataset(str(i)+"/constituents", data=sorted_constituents[str(i)])
      outfile.create_dataset(str(i)+"/hlvs", data=sorted_hlvs[str(i)])
      #sorted_constituents.update({str(i):[]})
    #outfile.create_dataset("hlvs", data=hlvs_total)
    outfile.close()
       
    #f = open("FTAG_LSTM.pkl", "wb")
    #pickle.dump(sorted_constituents, f)
    #f.close()
    
    
    for i in range(n_consts+1):      
      if(len(sorted_constituents[str(i)]) > 0):
    #    np.save("Sorted_Constituents/test_sorted_"+str(i)+".npy", sorted_constituents[str(i)])
        print(str(i) + " Consts: " + str(len(sorted_constituents[str(i)])))
    
    
    print(sorted_constituents["2"])
    print(np.shape(sorted_constituents["2"]))
    #np.save("test_sorted.npy", sorted_constituents[2])










